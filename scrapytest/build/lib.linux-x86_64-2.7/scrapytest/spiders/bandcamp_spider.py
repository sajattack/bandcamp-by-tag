import scrapy
import json
from math import ceil

from scrapytest.items import ArbitraryItem

api_url_format = "http://151.101.65.28/api/discover/3/get_web?t=%s&s=top&p=%s"
# bandcamp.com ip address to save dns lookups

class BandcampSpider(scrapy.Spider):
    name = "bandcamp"
    allowed_domains = ['bandcamp.com', '151.101.65.28']
    start_urls = [
        "http://bandcamp.com/tags"
    ]

    def parse(self, response):
        for link in response.css("#tags_cloud > a"):
            machine_tag_name = link.css("::attr('href')").extract()[0].replace("/tag/", "")
            human_tag_name = link.xpath("text()").extract()
            url = api_url_format % (machine_tag_name, 0)
            meta = {"machine": machine_tag_name, "human": human_tag_name}
            yield scrapy.Request(url, meta=meta, callback=self.parse_tag)

    def parse_tag(self, response):
        machine_tag_name = response.meta["machine"]
        json_obj = json.loads(response.body_as_unicode())
        total_count = json_obj.get("total_count")
        num_pages = int(ceil(total_count/48.0))
        for i in range(num_pages):
            url = api_url_format % (machine_tag_name, i)
            yield scrapy.Request(url, meta=response.meta, callback=self.parse_album)

    def parse_album(self, response):
        json_obj = json.loads(response.body_as_unicode())
        items = json_obj.get('items')
        for item in items:
            album = ArbitraryItem(item)
            album['id'] = str(album['id'])
            album['machine_tag_name'] = response.meta['machine']
            album['human_tag_name'] = response.meta['human']
            yield album
