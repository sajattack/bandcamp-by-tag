#!/usr/bin/python

from time import sleep
import requests
from bs4 import BeautifulSoup
import os
from math import ceil
import re
import codecs

# scrape list of tags
tagList = []
tagPage = requests.get("http://bandcamp.com/tags")
soup = BeautifulSoup(tagPage.text, "lxml")
for item in soup.select("#tags_cloud > a"):
    if(item.get_text() not in tagList and item.get_text() != ""):
        human_tag = item.get_text()
        # format the tag to what the api expects
        # http://stackoverflow.com/questions/6116978/python-replace-multiple-strings
        rep = {" & ": "-", "&": "-", " ": "-", "/": "-"}  # define desired replacements here
        rep = dict((re.escape(k), v) for k, v in rep.iteritems())
        pattern = re.compile("|".join(rep.keys()))
        machine_tag = pattern.sub(lambda m: rep[re.escape(m.group(0))], human_tag)
        tagList.append(machine_tag)
        try:
            os.mkdir(machine_tag)  # directory to save json
            with codecs.open(machine_tag + "/" + "human.txt", 'w+', "utf-8-sig") as f:
                f.write(human_tag)
        except Exception as e:
            print(e)

for index, tag in enumerate(tagList):
    print(tag + " " + str(index+1) + "/" + str(len(tagList)))

    req = requests.Response()
    while (req.status_code != 200):
        req = requests.get(
            "http://bandcamp.com/api/discover/3/get_web?t=" +
            tag + "&s=top&p=0")
    jsonObj = req.json()

    # each page contains at most 48 entries
    # find total num entries in page 0 and calculate num pages
    totalCount = jsonObj.get("total_count")
    print(totalCount)
    totalPages = int(ceil(totalCount/48.0))

    # iterate through pages, write to disk
    for page in range(0, totalPages):
        with open(tag+'/'+str(page), 'w+') as f:
            req = requests.Response()
            while (req.status_code != 200):  # lol
                req = requests.get(
                    "http://bandcamp.com/api/discover/3/get_web?t=" +
                    tag + "&s=top&p=" + str(page))
            f.write(req.text.encode("utf8"))
            print(str(page + 1) + "/" + str(totalPages) + " " +
                  str(req.status_code) + " " + str(len(req.text)))
    print(tag + " complete.")
