#!/usr/bin/env bash
curl -XPUT "http://localhost:9200/scrapy4" -d '
{
    "mappings" : {
      "items" : {
        "dynamic": "true",
        "properties" : {
          "art_id" : {
            "type" : "long"
          },
          "band_id" : {
            "type" : "long"
          },
          "bio_image" : {
            "properties" : {
              "height" : {
                "type" : "long"
              },
              "image_id" : {
                "type" : "long"
              },
              "width" : {
                "type" : "long"
              }
            }
          },
          "category" : {
            "type" : "string"
          },
          "featured_track" : {
            "properties" : {
              "duration" : {
                "type" : "double"
              },
              "encodings_id" : {
                "type" : "long"
              },
              "file" : {
                "properties" : {
                  "mp3-128" : {
                    "type" : "string",
                    "index": "not_analyzed"
                  }
                }
              },
              "id" : {
                "type" : "long"
              },
              "title" : {
                "type" : "string"
              }
            }
          },
          "genre_text" : {
            "type" : "string"
          },
          "human_tag_name" : {
            "type" : "string",
            "index" : "not_analyzed"
          },
          "is_preorder" : {
            "type" : "long"
          },
          "item_type_id" : {
            "type" : "string"
          },
          "location_text" : {
            "type" : "string"
          },
          "machine_tag_name" : {
            "type" : "string",
            "index" : "not_analyzed"
          },
          "primary_text" : {
            "type" : "string"
          },
          "publish_date" : {
            "type" : "string",
            "index": "not_analyzed"
          },
          "score" : {
            "type" : "double"
          },
          "secondary_text" : {
            "type" : "string"
          },
          "type" : {
            "type" : "string"
          },
          "url_hints" : {
            "properties" : {
              "custom_domain" : {
                "type" : "string"
              },
              "custom_domain_verified" : {
                "type" : "long"
              },
              "item_type" : {
                "type" : "string"
              },
              "slug" : {
                "type" : "string"
              },
              "subdomain" : {
                "type" : "string"
              }
            }
          }
        }
      }
    }
  }
}'
