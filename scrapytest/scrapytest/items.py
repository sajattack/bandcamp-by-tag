# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item


class ArbitraryItem(Item):
    def __setitem__(self, key, value):
        self._values[key] = value
        self.fields[key] = {}

# class BandcampTag(scrapy.Item):
#     machine_name = scrapy.Field()
#     human_name = scrapy.Field()
#     albums = scrapy.Field()
#     total_albums = scrapy.Field()
#
#
#class BandcampAlbum(scrapy.Item):
#    field = scrapy.Field()
