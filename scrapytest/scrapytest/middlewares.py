from scrapy.conf import settings
from scrapy.downloadermiddlewares.retry import RetryMiddleware
import telnetlib
import logging
import time
import random

logger = logging.getLogger(__name__)


class ProxyMiddleware(object):

    #Overide the request process by making it go through Tor
    def process_request(self, request, spider):
        request.meta['proxy'] = settings.get('HTTP_PROXY')


class RetryChangeProxyMiddleware(RetryMiddleware):

    last = 0
    timelimit = 15

    def _retry(self, request, reason, spider):
        settings = spider.settings

        if isinstance(reason, basestring):
            logger.debug('Valid retry, reason: ' + reason + ' for URL ' + request.url)
            t = time.time()
            diff = t - RetryChangeProxyMiddleware.last
            if diff > RetryChangeProxyMiddleware.timelimit:
                tn = telnetlib.Telnet('127.0.0.1', 9051)
                tn.read_until("Escape character is '^]'.", 2)
                tn.write('AUTHENTICATE "267765"\r\n')
                tn.read_until("250 OK", 2)
                tn.write("signal NEWNYM\r\n")
                tn.read_until("250 OK", 2)
                tn.write("quit\r\n")
                tn.close()
                #time.sleep(3)
                RetryChangeProxyMiddleware.last = t
                logger.info('Proxy changed! New last: %s' % time.strftime("%H:%M:%S"))
            else:
                logger.debug('Proxy not changed! Time difference is %s seconds' % ("{:.2f}".format(diff)))
            return RetryMiddleware._retry(self, request, reason, spider)
