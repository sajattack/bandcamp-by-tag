from __future__ import print_function
import datetime
from math import ceil
import grequests, requests
from bs4 import BeautifulSoup

DEFAULT_BURST_WINDOW = datetime.timedelta(seconds=5)
DEFAULT_WAIT_WINDOW = datetime.timedelta(seconds=15)
__CONCURRENT_LIMIT__ = 60


class BurstThrottle(object):
    max_hits = None
    hits = None
    burst_window = None
    total_window = None
    timestamp = None

    def __init__(self, max_hits, burst_window, wait_window):
        self.max_hits = max_hits
        self.hits = 0
        self.burst_window = burst_window
        self.total_window = burst_window + wait_window
        self.timestamp = datetime.datetime.min

    def throttle(self):
        now = datetime.datetime.utcnow()
        if now < self.timestamp + self.total_window:
            if (now < self.timestamp + self.burst_window) and (self.hits < self.max_hits):
                self.hits += 1
                return datetime.timedelta(0)
            else:
                return self.timestamp + self.total_window - now
        else:
            self.timestamp = now
            self.hits = 1
            return datetime.timedelta(0)


class MyHttpAdapter(requests.adapters.HTTPAdapter):
    throttle = None

    def __init__(self, pool_connections=requests.adapters.DEFAULT_POOLSIZE,
                 pool_maxsize=requests.adapters.DEFAULT_POOLSIZE, max_retries=requests.adapters.DEFAULT_RETRIES,
                 pool_block=requests.adapters.DEFAULT_POOLBLOCK, burst_window=DEFAULT_BURST_WINDOW,
                 wait_window=DEFAULT_WAIT_WINDOW):
        self.throttle = BurstThrottle(pool_maxsize, burst_window, wait_window)
        super(MyHttpAdapter, self).__init__(pool_connections=pool_connections, pool_maxsize=pool_maxsize,
                                            max_retries=max_retries, pool_block=pool_block)

    def send(self, request, stream=False, timeout=None, verify=True, cert=None, proxies=None):
        request_successful = False
        response = None
        while not request_successful:
            wait_time = self.throttle.throttle()
            while wait_time > datetime.timedelta(0):
                gevent.sleep(wait_time.total_seconds(), ref=True)
                wait_time = self.throttle.throttle()

            response = super(MyHttpAdapter, self).send(request, stream=stream, timeout=timeout,
                                                       verify=verify, cert=cert, proxies=proxies)

            if response.status_code != 503:
                request_successful = True

        return response


requests_adapter = MyHttpAdapter(
    pool_connections=__CONCURRENT_LIMIT__,
    pool_maxsize=__CONCURRENT_LIMIT__,
    max_retries=5,
    pool_block=False,
    burst_window=datetime.timedelta(seconds=5),
    wait_window=datetime.timedelta(seconds=20))

requests_session = requests.session()
requests_session.mount('http://', requests_adapter)
requests_session.mount('https://', requests_adapter)

api_url_format = "http://151.101.65.28/api/discover/3/get?t=%s&s=top&p=%d"
tagList = []
tagPage = requests.get("http://bandcamp.com/tags")
soup = BeautifulSoup(tagPage.text, "lxml")
for item in soup.select("#tags_cloud > a"):
    machine_tag = item.get('href').replace("/tag/", "")
    human_tag = item.get_text()
    if (machine_tag not in tagList and human_tag != ""):
        tagList.append(machine_tag)


def handle_page(response, **kwargs):
    print(response.url)

urls = []
for tag in tagList:
    response = requests.Response()
    while response.status_code != 200:
        response = requests.get(api_url_format % (tag, 0))
    print(response.url)
    jsonObj = response.json()
    totalCount = jsonObj.get("total_count")
    totalPages = int(ceil(totalCount / 48.0))
    for page in xrange(totalPages):
        urls.append(api_url_format % (tag, page))

rs = (grequests.get(url,
                   hooks={'response': handle_page},
                   session=requests_session)for url in urls)
for res in grequests.imap(rs, size=__CONCURRENT_LIMIT__):
    print(res)


